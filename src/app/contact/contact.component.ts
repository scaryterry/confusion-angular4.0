import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormGroupDirective} from '@angular/forms'; 
import {Feedback, ContactType} from '../shared/feedback';
import {flyInOut, expand} from '../animations/app.animation';

import {FeedbackService} from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackcopy =  {'firstname':'',
  'lastname':'',
  'telnum':0,
  'email':'',
  'agree':false,
  'contacttype':'none',
  'message':''
};
  contactType = ContactType;
  errMess: string;
  flag: boolean = false;
  copyflag:boolean = false;

  formErrors = {
    'firstname':'',
    'lastname':'',
    'telnum':'',
    'email':''
  };

  

  validationMessages = {
    'firstname':{
      'required':'First Name is required',
      'minlength':'First Name cannot be shorter than 2 characters',
      'maxlength':'First Name cannot be longer than 25 characters'
    },
    'lastname':{
      'required':'Last Name is required',
      'minlength':'Last Name cannot be shorter than 2 characters',
      'maxlength':'Last Name cannot be longer than 25 characters'
    },
    'telnum':{
      'required':'Tel. Number is required',
      'pattern':'Number format is invalid'
    },
    'email':{
      'required':'Email is required',
      'email':'Email format is invalid'
    }
  };

  constructor(private fb: FormBuilder, private feedbackservice: FeedbackService) {

    this.createForm();
   }

  ngOnInit() {
  }

  createForm(): void
  {
    this.feedbackForm = this.fb.group({
      firstname:['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname:['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum:['', [Validators.required,Validators.pattern]],
      email:['', [Validators.required, Validators.email]],
      agree:false,
      contacttype:'None',
      message:''
    });

    this.feedbackForm.valueChanges
    .subscribe(data=>this.onValueChanged(data));
  }

  onValueChanged(data?: any)
  {
    if(!this.feedbackForm) {return;}
    const form = this.feedbackForm;
    for (const field in this.formErrors)
    {
      //Clear form errors, if any
      this.formErrors[field]='';
      const control = form.get(field);
      if(control && control.dirty && !control.valid)
      {
        const messages = this.validationMessages[field];
        for(const key in control.errors)
        {
          this.formErrors[field]+=messages[key] + ' ';
        }
      }
    }
  }

  onSubmit(formDirective: FormGroupDirective)
  {
    console.log("flag: "+ this.flag);
    this.feedback = this.feedbackForm.value;
    this.flag=!this.flag;
    this.feedbackservice.submitFeedback(this.feedback)
    .subscribe(
      feedback=>{
        this.feedback=feedback; 
        this.feedbackcopy=feedback; 
        this.flag=!this.flag;
        this.copyflag=!this.copyflag;
        console.log("flag: "+ this.flag +" copyflag: "+this.copyflag );
        setTimeout((function(){this.copyflag=!this.copyflag; console.log("after timeout copyflag:" + this.copyflag)}).bind(this),5000);  
      },errmess=>this.errMess=<any>errmess);

    console.log(this.feedback); 
    formDirective.resetForm();
    this.feedbackForm.reset({
      firstname:'',
      lastname:'',
      telnum:'',
      email:'',
      agree:false,
      contacttype:'None',
      message:''

    });
  }

}

/*

<mat-error> checks the validity of FormGroupDirective, not FormGroup, and resetting FormGroup does not reset FormGroupDirective. 
It's a bit inconvenient, but to clear <mat-error> you would need to reset FormGroupDirective as well.

@Anagh
md-error has no such issues, resetting the form will reset the FormGroupDirective as well. But not in angular 6. Hence FormGroupDirective has been added outside the course content.
Make Note of the same. Resetting FormGroupDirective will set the form to pristine. Without it, after submit and form.reset(), the form will flash red giving invalid errors.


also Look at bind in timeout. bind(this) ensures that variables outside the timeout functions are changed. else variables inside functions of timeout are local to the function.

*/
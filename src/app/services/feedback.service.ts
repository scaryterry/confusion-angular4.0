import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';

import {Feedback} from "../shared/feedback";

import {Restangular} from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private restangular: Restangular) { }
  
  getFeedbacks():Observable<Feedback[]> {
    return this.restangular.all('feedback').getList();
  }

  getFeedback(id: number):Observable<Feedback> {
    return this.restangular.one('feedback',id).get();
  }

  submitFeedback(feedback:Feedback){

    return this.restangular.all('feedback').post(feedback);

  }
}

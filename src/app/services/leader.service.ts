import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';

import {delay} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';

import {RestangularModule, Restangular} from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private restangular: Restangular) { }

  getLeaders(): Observable<Leader []> {
    return this.restangular.all('leaders').getList();
}

  getLeader(id: number): Observable<Leader> {
    return this.restangular.one('leaders',id).get();
}

  getFeaturedLeader(): Observable<Leader> {
    return this.restangular.all('leaders').getList({featured:true})
    .map(leaders=>leaders[0]);
  }
}

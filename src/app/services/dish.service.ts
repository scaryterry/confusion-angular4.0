import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import {Http, Response} from '@angular/http';

import {delay} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Dish } from '../shared/dish';
import {baseURL} from '../shared/baseurl';
import {ProcessHttpMsgService} from './process-http-msg.service';

import {RestangularModule, Restangular} from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private restangular: Restangular, private processHttpMsgService: ProcessHttpMsgService) { }

  getDishes(): Observable<Dish[]> {
    return this.restangular.all('dishes').getList();

  }

  getDish(id: number): Observable<Dish> {
    return this.restangular.one('dishes',id).get();

  }

  getFeaturedDish(): Observable<Dish> {
    return this.restangular.all('dishes').getList({featured: true})
    .map(dishes => dishes[0]);
  }

  getDishIds():Observable<number[] | any >{
    return this.getDishes()
    .map(dishes=>{return dishes.map(dish=>dish.id);})
    .catch(error => {return error;})
  ;
  }

}
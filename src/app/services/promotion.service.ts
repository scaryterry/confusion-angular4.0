import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';

import {delay} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';

import {RestangularModule, Restangular} from 'ngx-restangular';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private restangular: Restangular) { }

  getPromotions(): Observable<Promotion[]> {
    return this.restangular.all('promotions').getList();
}

  getPromotion(id: number): Observable<Promotion> {
    return this.restangular.one('promotions',id).get();
}

  getFeaturedPromotion(): Observable<Promotion> {
    return this.restangular.all('promotions').getList({featured:true})
    .map(promotions=>promotions[0])
  }
}
